package org.taruts.djig.configurationProperties;

import lombok.Getter;

@Getter
public enum BuildSystem {
    MAVEN,
    GRADLE
}
