package org.taruts.djig.configurationProperties;

public enum HostingType {
    GitHub,
    GitLab,
}
