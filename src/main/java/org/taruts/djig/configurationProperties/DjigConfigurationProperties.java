package org.taruts.djig.configurationProperties;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class DjigConfigurationProperties {

    public static final String PREFIX = "djig";

    Map<String, DynamicProject> dynamicProjects = new HashMap<>();
    Map<String, BaseProjectProperties> hostings = new HashMap<>();
    BaseProjectProperties common;

    Hook hook = new Hook();

    Controller controller = new Controller();

    @Getter
    @Setter
    public static class BaseProjectProperties {
        String username;
        String password;
        String branch;
        String dynamicInterfacePackage;
        BuildType buildType;
        HostingType hostingType;
    }

    @Getter
    @Setter
    public static class DynamicProject extends BaseProjectProperties {
        String url;
    }

    @Getter
    @Setter
    public static class Hook {
        String protocol = "http";
        String host;
        int port = -1;
        boolean sslVerification = false;
        String secret;
    }

    @Getter
    @Setter
    public static class Controller {

        Element refresh = new Element("refresh");
        Element dynamicProject = new Element("dynamic-project");

        @Getter
        @Setter
        public static class Element {
            String path;

            public Element(String path) {
                this.path = path;
            }
        }
    }
}
