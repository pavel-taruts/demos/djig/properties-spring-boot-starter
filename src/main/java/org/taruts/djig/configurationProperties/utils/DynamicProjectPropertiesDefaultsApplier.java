package org.taruts.djig.configurationProperties.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties.BaseProjectProperties;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties.DynamicProject;
import org.taruts.gitUtils.hosting.HostingNameExtractor;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class DynamicProjectPropertiesDefaultsApplier {

    public static DynamicProject applyDefaults(DjigConfigurationProperties djigConfigurationProperties) {
        return applyDefaults(djigConfigurationProperties, (String) null);
    }

    public static DynamicProject applyDefaults(DjigConfigurationProperties djigConfigurationProperties, String projectName) {
        DynamicProject sourceDynamicProjectProperties = getUserProjectPropertiesSafe(djigConfigurationProperties, projectName);
        return applyDefaults(djigConfigurationProperties, sourceDynamicProjectProperties);
    }

    public static DynamicProject applyDefaults(DjigConfigurationProperties djigConfigurationProperties, DynamicProject sourceDynamicProjectProperties) {
        DynamicProject targetDynamicProjectProperties = new DynamicProject();
        applyDefaults(djigConfigurationProperties, sourceDynamicProjectProperties, targetDynamicProjectProperties);
        return targetDynamicProjectProperties;
    }

    public static void applyDefaults(
            DjigConfigurationProperties djigConfigurationProperties,
            DynamicProject sourceDynamicProjectProperties,
            DynamicProject targetDynamicProjectProperties
    ) {
        List<BeanWrapper> sourceWrappers = getSourceBeanWrappers(djigConfigurationProperties, sourceDynamicProjectProperties);

        BeanWrapperImpl targetWrapper = new BeanWrapperImpl(targetDynamicProjectProperties);

        Arrays
                .stream(targetWrapper.getPropertyDescriptors())
                .forEach(propertyDescriptor -> {
                    String propertyName = propertyDescriptor.getName();
                    if ("class".equals(propertyName)) {
                        return;
                    }
                    Object propertyValue = getValueFromSourceWrappers(propertyDescriptor, sourceWrappers);
                    targetWrapper.setPropertyValue(propertyName, propertyValue);
                });

        if (StringUtils.isBlank(targetDynamicProjectProperties.getBranch())) {
            targetDynamicProjectProperties.setBranch("master");
        }
    }

    private static List<BeanWrapper> getSourceBeanWrappers(DjigConfigurationProperties djigConfigurationProperties, DynamicProject dynamicProjectProperties) {

        BaseProjectProperties hostingProperties = getHostingProperties(djigConfigurationProperties, dynamicProjectProperties);

        BaseProjectProperties commonProperties = djigConfigurationProperties.getCommon();

        List<BeanWrapper> sourceWrappers = Stream
                .of(
                        dynamicProjectProperties,
                        hostingProperties,
                        commonProperties
                )
                .filter(Objects::nonNull)
                .<BeanWrapper>map(BeanWrapperImpl::new)
                .toList();

        return sourceWrappers;
    }

    private static BaseProjectProperties getHostingProperties(
            DjigConfigurationProperties djigConfigurationProperties,
            DynamicProject dynamicProjectProperties
    ) {
        String projectUrl = dynamicProjectProperties.getUrl();
        String hostingName = HostingNameExtractor.extractHostingName(projectUrl);
        return djigConfigurationProperties.getHostings().get(hostingName);
    }

    private static DynamicProject getUserProjectPropertiesSafe(
            DjigConfigurationProperties djigConfigurationProperties,
            String dynamicProjectName
    ) {
        if (dynamicProjectName == null) {
            return new DynamicProject();
        } else {
            Map<String, DynamicProject> dynamicProjects = djigConfigurationProperties.getDynamicProjects();
            if (dynamicProjects.containsKey(dynamicProjectName)) {
                return dynamicProjects.get(dynamicProjectName);
            } else {
                throw new NullPointerException("There is no project %s in the Spring Boot configuration properties".formatted(
                        dynamicProjectName
                ));
            }
        }
    }

    private static Object getValueFromSourceWrappers(PropertyDescriptor propertyDescriptor, List<BeanWrapper> sourceWrappers) {

        String propertyName = propertyDescriptor.getName();

        Predicate<Object> isNotEmptyPredicate = propertyDescriptor.getPropertyType() == String.class
                ? DynamicProjectPropertiesDefaultsApplier::isNotBlank
                : Objects::nonNull;

        return sourceWrappers
                .stream()
                .map(sourceWrapper -> {
                    if (sourceWrapper.isReadableProperty(propertyName)) {
                        return sourceWrapper.getPropertyValue(propertyName);
                    } else {
                        return null;
                    }
                })
                .filter(isNotEmptyPredicate)
                .findFirst()
                .orElse(null);
    }

    private static boolean isNotBlank(Object value) {
        return StringUtils.isNotBlank((String) value);
    }

    public static void applyDefaultsForAllProjectsInPlace(DjigConfigurationProperties djigConfigurationProperties) {
        djigConfigurationProperties.getDynamicProjects().forEach((projectName, projectProperties) -> {
            applyDefaults(djigConfigurationProperties, projectProperties, projectProperties);
        });
    }
}
