package org.taruts.djig.configurationProperties.utils;

import lombok.SneakyThrows;
import org.taruts.djig.configurationProperties.HostingType;

import java.net.URI;
import java.net.URL;

public class HostingTypeDetector {

    @SneakyThrows
    public static HostingType detect(String url) {
        return detect(new URI(url));
    }

    private static HostingType detect(URI uri) {
        if ("github.com".equals(uri.getHost())) {
            return HostingType.GitHub;
        } else {
            return HostingType.GitLab;
        }
    }

    @SneakyThrows
    public static HostingType detect(URL url) {
        return detect(url.toURI());
    }
}
