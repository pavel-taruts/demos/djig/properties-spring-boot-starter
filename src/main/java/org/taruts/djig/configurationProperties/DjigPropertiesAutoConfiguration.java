package org.taruts.djig.configurationProperties;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@AutoConfiguration
@EnableConfigurationProperties(DjigConfigurationProperties.class)
public class DjigPropertiesAutoConfiguration {
}
